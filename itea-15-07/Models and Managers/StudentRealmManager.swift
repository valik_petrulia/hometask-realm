//
//  StudentRealmManager.swift
//  itea-15-07
//
//  Created by Валентин Петруля on 7/16/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation
import RealmSwift

class StudentRealmManager {
    
    static let sharedInstance: StudentRealmManager = {
        let instance = StudentRealmManager()
        return instance
    }()
    
    let realm = try! Realm()
    
    func setData() -> [StudentRealmModel] {
        
        deleteAll(array:  Array(realm.objects(StudentRealmModel.self)))
        
        let studentRealm1 = StudentRealmModel()
        studentRealm1.name = "Dex"
        studentRealm1.surname = "Jordan"
        studentRealm1.sex = "Male"
        studentRealm1.age = "34"
        studentRealm1.city = "London"
        writeObject(realmObject: studentRealm1)
        
        let studentRealm2 = StudentRealmModel()
        studentRealm2.name = "Jim"
        studentRealm2.surname = "Calloway"
        studentRealm2.sex = "Male"
        studentRealm2.age = "58"
        studentRealm2.city = "Washington"
        writeObject(realmObject: studentRealm2)
        
        let studentRealm3 = StudentRealmModel()
        studentRealm3.name = "Ann"
        studentRealm3.surname = "Jaymes"
        studentRealm3.sex = "Female"
        studentRealm3.age = "29"
        studentRealm3.city = "New York"
        writeObject(realmObject: studentRealm3)
        
        
        let array = Array(realm.objects(StudentRealmModel.self))
        
        return array
    }
    
    func writeObject(realmObject: StudentRealmModel) {
        do {
            try realm.write {
                realm.add(realmObject)
            }
        } catch {
            print("Ooops, error!")
        }
    }
    
    func deleteAll(array: [StudentRealmModel]) {
        if !array.isEmpty {
            try! realm.write {
                realm.delete(array)
            }
        }
    }
}

