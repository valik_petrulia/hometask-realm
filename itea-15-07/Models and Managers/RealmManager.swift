//
//  RealmManager.swift
//  itea-15-07
//
//  Created by Валентин Петруля on 7/15/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    
    static let sharedInstance: RealmManager = {
        let instance = RealmManager()
        return instance
    }()
    
    let realm = try! Realm()
    
    func setData() -> [RealmModel] {
        
        deleteAll(array:  Array(realm.objects(RealmModel.self)))
        
        let realmObject1 = RealmModel()
        realmObject1.name = "First"
        realmObject1.title = "Realm \(realmObject1.name )"
        writeObject(realmObject: realmObject1)
        
        let realmObject2 = RealmModel()
        realmObject2.name = "Second"
        realmObject2.title = "Realm \(realmObject2.name )"
        writeObject(realmObject: realmObject2)
        
        let realmObject3 = RealmModel()
        realmObject3.name = "Third"
        realmObject3.title = "Realm \(realmObject3.name )"
        writeObject(realmObject: realmObject3)
        
        let realmObject4 = RealmModel()
        realmObject4.name = "Fourth"
        realmObject4.title = "Realm \(realmObject4.name )"
        writeObject(realmObject: realmObject4)
        
        let realmObject5 = RealmModel()
        realmObject5.name = "Fifth"
        realmObject5.title = "Realm \(realmObject5.name )"
        writeObject(realmObject: realmObject5)
        
        let array = Array(realm.objects(RealmModel.self))
        
        return array
    }
    
    func writeObject(realmObject: RealmModel) {
        do {
            try realm.write {
                realm.add(realmObject)
            }
        } catch {
            print("Ooops, error!")
        }
    }
    
    func deleteAll(array: [RealmModel]) {
        if !array.isEmpty {
            try! realm.write {
                realm.delete(array)
            }
        }
    }
}

