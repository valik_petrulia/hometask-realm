//
//  RealmModel.swift
//  itea-15-07
//
//  Created by Валентин Петруля on 7/15/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation
import RealmSwift

class RealmModel: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var name: String = ""
}
