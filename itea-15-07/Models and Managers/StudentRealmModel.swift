//
//  StudentRealmModel.swift
//  itea-15-07
//
//  Created by Валентин Петруля on 7/16/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation
import RealmSwift

class StudentRealmModel: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var surname: String = ""
    @objc dynamic var sex: String = ""
    @objc dynamic var age: String = ""
    @objc dynamic var city: String = ""
}
