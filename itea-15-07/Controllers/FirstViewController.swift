//
//  FirstViewController.swift
//  itea-15-07
//
//  Created by Валентин Петруля on 7/15/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit
import SCLAlertView

class FirstViewController: UIViewController {

    var studentsRealm: [StudentRealmModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        studentsRealm = StudentRealmManager.sharedInstance.setData()
        studentsRealm = Array(StudentRealmManager.sharedInstance.realm.objects(StudentRealmModel.self))

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapShowAllButton(_ sender: Any) {
        if studentsRealm.count != 0 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "RealmViewController") as! RealmViewController
            vc.studentsRealm = studentsRealm
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let alert = UIAlertController(title: "Warning!", message: "Student's list is empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func didTapClearButton(_ sender: Any) {
        let alert = UIAlertController(title: "Warning!", message: "Are you sure?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            StudentRealmManager.sharedInstance.deleteAll(array: self.studentsRealm)
            self.studentsRealm = []
            let alertView = SCLAlertView()
            alertView.iconTintColor = UIColor.green
            let _: SCLAlertViewResponder = alertView.showSuccess("Successfully deleted!", subTitle: "")
        }))
        self.present(alert, animated: true)
    }
    
    @IBAction func didTapAddButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddViewController") as! AddViewController
        vc.studentsRealm = studentsRealm
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
