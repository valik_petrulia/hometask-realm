//
//  RealmViewController.swift
//  itea-15-07
//
//  Created by Валентин Петруля on 7/15/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class RealmViewController: UIViewController {
    
    var studentsRealm: [StudentRealmModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "StudentRealmCell", bundle: nil), forCellReuseIdentifier: "StudentRealmCell")
        self.navigationController?.isNavigationBarHidden = true
        tableView.reloadData()
    }
    @IBAction func didTapBackButton(_ sender: Any) {
        if let vc = self.navigationController?.viewControllers[0] as? FirstViewController {
            vc.studentsRealm = studentsRealm
            self.navigationController?.popToViewController(vc, animated: true)
        }
    }
    
}

extension RealmViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentsRealm.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentRealmCell", for: indexPath) as! StudentRealmCell
        cell.update(student: studentsRealm[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
}
