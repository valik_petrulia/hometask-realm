//
//  AddViewController.swift
//  itea-15-07
//
//  Created by Валентин Петруля on 7/15/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit
import RealmSwift

class AddViewController: UIViewController {
    
    var studentsRealm: [StudentRealmModel] = []
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var addStudentButton: UIButton!
    
    @IBOutlet weak var outsideMaleView: UIView!
    @IBOutlet weak var insideMaleView: UIView!
    
    
    @IBOutlet weak var outsideFemaleView: UIView!
    @IBOutlet weak var insideFemaleView: UIView!
    
    
    var isMaleFlag = false
    var isFemaleFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addStudentButton.layer.cornerRadius = 15
        setUpView(view: outsideMaleView)
        setUpView(view: insideMaleView)
        setUpView(view: outsideFemaleView)
        setUpView(view: insideFemaleView)
        ageTextField.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        ageTextField.resignFirstResponder()
    }
    
    func setUpView(view: UIView) {
        view.clipsToBounds = true
        view.backgroundColor = .white
        view.layer.cornerRadius = view.frame.size.width/2
        view.layer.masksToBounds = true
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.black.cgColor
    }

    @IBAction func didTapMaleButton(_ sender: Any) {
        if !isMaleFlag {
            isMaleFlag = true
            isFemaleFlag = false
            insideMaleView.backgroundColor = .black
            insideFemaleView.backgroundColor = .white
        }
    }
    
    @IBAction func didTapFemaleButton(_ sender: Any) {
        if !isFemaleFlag {
            isFemaleFlag = true
            isMaleFlag = false
            insideFemaleView.backgroundColor = .black
            insideMaleView.backgroundColor = .white
        }
    }
    
    @IBAction func didTapAddStudentButton(_ sender: Any) {
        let newStudentRealm = StudentRealmModel()
        if nameTextField.text == "" || surnameTextField.text == "" || !isMaleFlag && !isFemaleFlag || ageTextField.text == "" || cityTextField.text == "" {
            let alert = UIAlertController(title: "Warning!", message: "Fill in all fields", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        } else {
            newStudentRealm.name = nameTextField.text!
            newStudentRealm.surname = surnameTextField.text!
            if isMaleFlag {
                newStudentRealm.sex = "Male"
            } else {
                newStudentRealm.sex = "Female"
            }
            newStudentRealm.age = ageTextField.text!
            newStudentRealm.city = cityTextField.text!
            StudentRealmManager.sharedInstance.writeObject(realmObject: newStudentRealm)
            studentsRealm.append(newStudentRealm)
            let vc = storyboard?.instantiateViewController(withIdentifier: "RealmViewController") as! RealmViewController
            vc.studentsRealm = studentsRealm
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        if let vc = self.navigationController?.viewControllers[0] as? FirstViewController {
            vc.studentsRealm = studentsRealm
            self.navigationController?.popToViewController(vc, animated: true)
        }
    }
    
}

extension AddViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        return true
    }
}
