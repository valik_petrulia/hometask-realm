//
//  RealmCell.swift
//  itea-15-07
//
//  Created by Валентин Петруля on 7/15/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class RealmCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(realm: RealmModel) {
        titleLabel.text = realm.title 
        nameLabel.text = realm.name 
    }
    
}
