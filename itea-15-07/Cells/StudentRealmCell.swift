//
//  StudentRealmCell.swift
//  itea-15-07
//
//  Created by Валентин Петруля on 7/18/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class StudentRealmCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(student: StudentRealmModel) {
        nameLabel.text = student.name
        surnameLabel.text = student.surname
        sexLabel.text = student.sex
        ageLabel.text = student.age
        cityLabel.text = student.city
    }
    
}
